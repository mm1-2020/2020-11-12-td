Programme :

-> Mettre en forme son document avec CSS (suite)
1. les propriétés à valeurs multiples
2. les unités en CSS - dimensions
3. le positionnement

-> A vous !
1. base du document folio en HTML
2. charger une feuille de style
3. faire un layout basique avec 1 entete + 2 sections et 1 footer
4. premiere section, placer 4 images
5. Seconde section placer 3 colonnes de texte avec un titre

-> Etudes de cas CSS
1. le roll-over sans JS avec les pseudo classes
2. on s'amuse avec les pseudos éléments
2. 3 colonnes égales avec flex !



-> Liens ressources
Général CSS
https://www.w3schools.com/cssref/css_units.asp
http://lukeangel.co/developement/css/learn-css-in-16-post-it-notes/
https://developer.mozilla.org/fr/docs/Web/CSS/Pseudo-classes
https://developer.mozilla.org/fr/docs/Web/CSS/Pseudo-%C3%A9l%C3%A9ments
https://caniuse.com/

